variable "gcp_project_id" {}
variable "gcp_region" {}
variable "gcp_zone" {}
variable "gitlab_url" {}
variable "ci_token" {}
variable "ci_runner_gitlab_tags" {}
variable "ci_runner_disk_size" {}
variable "ci_runner_instance_type" {}
